# ----------------------------------------------------
# File: PongGame.py
# ----------------------------------------------------
# Author: Sayyeda Saadia Razvi, Sarah Elias, Jake Lewis, BitBucket handle (SaadiaR)
# ----------------------------------------------------
# Plaftorm: Windows 7
# Environment: Python 2.7 
# Libaries: MatPlotLib 1.3.1
#           Tkinter $Revision: 81008 $
#           Math
#           Random
#           Itertools
# ----------------------------------------------------
# Description: 
# This program creates a Pong Game using object oriented classes
# A Ball class and Paddle class create mathematical models of ball and paddles respectively
# A Pong class (using tkinter) creates physical models in GUI 
# Pong class also handles interaction/collision of ball with board and paddles, etc
# ---------------------------------------------------

from random import choice
import itertools
from math import sin, cos, pi
import Tkinter as tk
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import sys, winsound


class Ball():
    def __init__(self, board_width, board_height, time_interval):
        # create ball with specified radius, color and other initial conditions
        self.radius = 10
        self.color = "red"
        self.initial_conditions(board_width, board_height)
        self.update_position(time_interval)

    def initial_conditions(self, board_width, board_height):
        # gives ball velocity with random angle
        # ensures reasonable angle chosen and ball never moves just vertically
        Angle_range = list(itertools.chain(xrange(30,150), xrange(210,330))) 
        Angle = choice(Angle_range)
        self.xVelocity = 400*sin(Angle * pi/180)
        self.yVelocity = 400*cos(Angle * pi/180)
        # centers ball on the board
        self.xPos = board_width/2
        self.yPos = board_height/2

    def update_position(self, time_interval): 
        #uses the velocity to update the position
        self.xPos += self.xVelocity * time_interval
        self.yPos += self.yVelocity * time_interval

    def ball_wall_response(self, board_height):
        #update position when the ball hits the walls so that it begins at wall
        if self.yPos < board_height/2: #collision with top wall (canvas defined from upper left corner)
            self.yPos = self.radius
        elif self.yPos > board_height/2: #collision with bottom wall
            self.yPos = board_height - self.radius
        #update velocity so ball is reflected off wall
        self.yVelocity *= -1

    def ball_paddle_response(self, board_width, paddle_width):
        # update position of ball so it just touches the paddle 
        if self.xPos < board_width/2: #collision with left paddle
            self.xPos = self.radius + paddle_width
        elif self.xPos > board_width/2: #collision with right paddle
            self.xPos = board_width - self.radius - paddle_width
        # then update velocity
        self.xVelocity *= -1
        #what about other shapes?

    def bounce(self, surface_normal):
        #we would like to create a general response function for bouncing. Will do for part 2.
        pass

    def ball_miss_response(self, board_width, board_height):
        # reinitialize ball w/ centered position and new velocity
        self.initial_conditions(board_width, board_height)

    def increase_speed(self):
        # increases velocity of ball
            self.xVelocity *= 1.5
            self.yVelocity *= 1.5

    #==========================================================
    def gravitational_field_response(self):
        #AAAAHHHHHHH
        pass
    def ball_obstacle_response(self):
        #will probably have to make functions for each type of obstacle
        pass
    def portal_response(self):
        #change position
        pass
    
#========================================================================
class Monster(Ball):
    def __init__(self):
        Ball.__init__(self)
    # do later
#========================================================================
    
class Paddle():
    def __init__(self, xPos, color, board_height, up, down):
        # Initialize paddle
        self.length = 70
        self.width = 20
        self.color = color
        self.velocity = 5
        self.xPos = xPos
        self.center_paddle(board_height)
        self.up_key = up
        self.down_key = down

    def move(self, buttons, board_height):
        # makes up and down buttons for paddles so that pushing and releasing both change velocity
        # this way if you press both buttons you won't move
        if (self.up_key in buttons and self.down_key not in buttons) and (self.yPos - self.length/2 > 0):
            self.yPos -= self.velocity
        elif (self.down_key in buttons and self.up_key not in buttons) and (self.yPos + self.length/2 < board_height):
            self.yPos += self.velocity

    def center_paddle(self, board_height):
        #position of mathematical paddle defined from the middle of the outside side
        self.yPos = board_height/2

    #======================================================
    def change_shape(self):
        #change shape of paddle
        pass
    def change_speed(self):
        #change velocity of paddle
        pass

#========================================================================        
    
class Pong(tk.Frame):
    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.master = master
        # ====================================
        self.Initialize_Objects()           # creates ball and paddles from their respective classes
        #=====================================
        #Tkinter initialization
        # controls keyboard
        self.buttons = set()
        self.bind_all('<KeyPress>', lambda event: self.buttons.add(event.keysym))
        self.bind_all('<KeyRelease>', lambda event: self.buttons.discard(event.keysym))

        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)
        
        self.Create_Widgets()
        self.Draw_Board()
        self.pack()

        #Time loop for animation
        self.Update_Ball_Location()
    #==========================================================    

    def Initialize_Objects(self):
        #Variables for game
        self.board_width = 600
        self.board_height = 600
        self.time_interval = .01
        self.player1_score = 0
        self.player2_score = 0
        self.hits = 0 # number of consecutive hits by players
        self.reset = False # checks if program has been reset
        #===================================
        # Create ball and paddle objects from their classes
        self.b1 = Ball(self.board_width, self.board_height, self.time_interval)
        self.player1 = Paddle(0, "blue", self.board_height, 'w', 's')
        self.player2 = Paddle(self.board_width, "green", self.board_height, 'Up', 'Down')
        
    #==========================================================

    def Create_Widgets(self):
        # buttons for restart and quit
        self.restart = tk.Button(self, text="Restart", command=self.restart).grid(column=0,row=0, sticky = 'W')
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=1,row=0)

    def Draw_Board(self):
        #create canvas
        self.w = tk.Canvas(self, width=self.board_width, height=self.board_height, bg="black", relief="raised")
        self.w.grid(column=0, row=1, columnspan=2)
        
        board_divider = self.w.create_line( self.board_width/2, self.board_height, self.board_width/2, 0, fill="white", dash=(4,4))
        
        #creates physical ball and paddles as well as the score keeper (text)
        self.CreateTKBall()
        self.CreateTKPaddle()
        self.Update_score()

    def CreateTKBall(self):
        # physical ball. Positions defined by upper left and lower right corners of a bounding box
        self.ball1 = self.w.create_oval( self.b1.xPos-self.b1.radius, 
                                         self.b1.yPos+self.b1.radius, 
                                         self.b1.xPos+self.b1.radius, 
                                         self.b1.yPos-self.b1.radius, 
                                         fill = self.b1.color)


    def CreateTKPaddle(self):
        # physical paddles. Positions defined by upper left and lower right corners of rectangle. 
        self.paddle1 = self.w.create_rectangle( 0, 
                                              ( self.player1.yPos - self.player1.length/2 ), 
                                                self.player1.width, 
                                              ( self.player1.yPos + self.player1.length/2 ), 
                                                fill = self.player1.color)
        self.paddle2 = self.w.create_rectangle( (self.board_width - self.player2.width), 
                                                (self.player2.yPos - self.player1.length/2), 
                                                 self.board_width, 
                                                (self.player2.yPos + self.player2.length/2), 
                                                 fill = self.player2.color)

    def Update_score(self):
        # score displayer
        p1_score_string = "Player 1: %s" % self.player1_score
        p2_score_string = "Player 2: %s" % self.player2_score
        self.p1score = self.w.create_text( self.board_width/2 - 60, 20, text=p1_score_string, fill="white")
        self.p2score = self.w.create_text( self.board_width/2 + 60, 20, text=p2_score_string, fill="white")

    def restart(self):
        # restarts game by setting reset state to True, then reinitializing objects and board
        self.reset = True
        self.Initialize_Objects()
        self.Draw_Board()

    #===========================================================================
    # Paddle Functions
    def update_paddle_math(self):
        # for updating mathematical model
        self.player1.move(self.buttons, self.board_height) #math
        self.player2.move(self.buttons, self.board_height)

    def update_paddle_position(self):
        # for updating tkinter model of paddle1 and paddle2
        self.w.move(self.paddle1, 0, self.player1.yPos - self.w.coords(self.paddle1)[1] - self.player1.length/2) #physical      
        self.w.move(self.paddle2, 0, self.player2.yPos - self.w.coords(self.paddle2)[1] - self.player1.length/2)

    #============================================================================
    # Event functions

    def closest_distance(self):
        #we would like to create a function that detects the closest distance between two objects. will do for part 2
        pass

    def ball_wall_event(self):
        #returns TRUE if ball hits either top or bottom wall. otherwise FALSE
        bottom = self.b1.yPos >= self.board_height - self.b1.radius
        top = self.b1.yPos <= self.b1.radius
        return top or bottom

    def ball_paddle_event(self):
        #returns TRUE if ball hits paddles (meaning ball is in the x and y positions overlapping the paddle). otherwise FALSE
        ball_left_hit = self.b1.xPos <= self.player1.width + self.b1.radius
        ball_right_hit = self.b1.xPos >= self.board_width - self.player2.width - self.b1.radius
        paddle1_there = (self.player1.yPos - self.player1.length/2) <= self.b1.yPos <= (self.player1.yPos + self.player1.length/2)
        paddle2_there = (self.player2.yPos - self.player2.length/2) <= self.b1.yPos <= (self.player2.yPos + self.player2.length/2)
        return (ball_left_hit and paddle1_there) or (ball_right_hit and paddle2_there)

    def ball_miss_event(self):
        #returns TRUE if ball misses the paddle and it "hits" the left or right walls. otherwise FALSE
        ball_left = self.b1.xPos <= 0 - self.b1.radius
        ball_right = self.b1.xPos >= self.board_width + self.b1.radius
        return ball_left or ball_right 

    def ball_miss_response(self): 
        winsound.Beep(400, 95)  # play beep
        #checks to see who gains point 
        if self.b1.xPos < self.board_width/2: # ball on left side of board
            self.player2_score += 1
            self.hits = 0
        if self.b1.xPos > self.board_width/2: # ball on right side of board
            self.player1_score += 1
            self.hits = 0 

        # resets mathematical ball and paddles   
        self.b1.ball_miss_response(self.board_width, self.board_height)
        self.player1.center_paddle(self.board_height)
        self.player2.center_paddle(self.board_height)

        # resets physical ball and paddles
        self.w.delete(self.ball1, self.paddle1, self.paddle2, self.p1score, self.p2score)
        self.CreateTKBall()
        self.CreateTKPaddle()
        # updates score
        self.Update_score()
        # self.Status = self.w.create_text( self.board_width/2, self.board_height-50, text="Ball Missed!", fill="white")

    def ball_paddle_response(self):
        sys.stdout.write("\a") # play beep                
        # calls function responsible for ball's response to hitting paddle
        self.b1.ball_paddle_response(self.board_width, self.player1.width)
        self.w.delete(self.ball1)
        self.CreateTKBall()

        # updates number of consecutive hits to increase game difficulty after every 5 hits
        self.hits += 1
        if self.hits % 5 ==0: # that is, 5, 10, 15...
            self.b1.increase_speed()           

    def event_handler(self):
        # Event detection lists
        self.events = [self.ball_wall_event, 
                       self.ball_paddle_event, 
                       self.ball_miss_event]
        self.responses = [lambda: self.b1.ball_wall_response(self.board_height),
                          self.ball_paddle_response,
                          self.ball_miss_response]

        # checks for an event and calls corresponding event response
        for event, response in zip(self.events, self.responses):
            if event():
                response()

    #=========================================================================================
    # Running a recursive function to update mathematical model, check for events, re-update math, then draw/move in tkinter 
    def Update_Ball_Location(self):
        # checks if program is in a state of reset, if so restarts program
        if self.reset == True:
            self.restart()
            self.reset = False

        # updates mathematical position
        self.b1.update_position(self.time_interval)
        self.update_paddle_math()

        # runs the event handler to see if mathematical positions require a response
        self.event_handler()

        # updates physical position of the ball and paddle
        dx = self.b1.xVelocity * self.time_interval
        dy = self.b1.yVelocity * self.time_interval
        tt = int(self.time_interval * 1000) # ms (milli seconds)
        self.update_paddle_position() 
        self.w.move(self.ball1, dx, dy)

        # time loop to continuously update game
        self.w.after(tt, self.Update_Ball_Location)

       
root = tk.Tk()
pong_game = Pong(master=root)
pong_game.master.title("Play Pong!")
pong_game.mainloop()
root.destroy() 
    
